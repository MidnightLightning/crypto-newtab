'use strict';

var pairsToIds = {
  'XETHZUSD': 'eth',
  'XXBTZUSD': 'btc',
  'XLTCZUSD': 'ltc'
};

function getPrices() {
  return new Promise(function(resolve, reject) {
    var tickerApi = 'https://api.kraken.com/0/public/Ticker?pair=';
    tickerApi += Object.keys(pairsToIds).join(',');

    var xhr = new XMLHttpRequest();
    xhr.open('GET', tickerApi);
    xhr.onload = function () {
      if (xhr.status === 200) {
        // Request returned 200
        resolve(xhr.responseText);
      } else {
        reject(xhr.status);
      }
    };
    xhr.send(null);
  });
}

function toDisplayNum(num, digits) {
  return num.toLocaleString(false, {
    maximumFractionDigits: digits,
    minimumFractionDigits: digits
  });
}
function buildDataRow(label, value) {
  return `<div class="data-row"><div class="label">${label}</div><div>${value}</div></div>`;
}


getPrices().then(function(rs) {
  var data = JSON.parse(rs);
  var prices = data.result;
  Object.keys(prices).map(function(pair) {
    var cssId = pairsToIds[pair];
    var container = document.getElementById(cssId);
    var currentPrice = parseFloat(prices[pair].p[0]);
    container.getElementsByClassName('current')[0].innerHTML = '$' + toDisplayNum(currentPrice, 2);
    var openPrice = parseFloat(prices[pair].o);
    var todayDelta = currentPrice - openPrice;
    var todayRatio = (todayDelta/openPrice)*100;
    if (todayDelta >= 0) {
      container.getElementsByClassName('current-changes')[0].innerHTML = buildDataRow(
        'Today\'s change:',
        `<span class="up">$${toDisplayNum(todayDelta, 2)}</span> (<span class="up">${toDisplayNum(todayRatio, 3)}%</span>)`
      );
    } else {
      container.getElementsByClassName('current-changes')[0].innerHTML = buildDataRow(
        'Today\'s change:',
        `<span class="down">-$${toDisplayNum(todayDelta*-1, 2)}</span> (<span class="down">${toDisplayNum(todayRatio, 3)}%</span>)`
      );
    }
    var todayLow = parseFloat(prices[pair].l[0]);
    var todayHigh = parseFloat(prices[pair].h[0]);
    container.getElementsByClassName('meta')[0].innerHTML = buildDataRow(
      'Open:',
      '$' + toDisplayNum(openPrice, 2)
    ) + buildDataRow(
      'Low:',
      '$' + toDisplayNum(todayLow, 2)
    ) + buildDataRow(
      'High:',
      '$' + toDisplayNum(todayHigh, 2)
    );
  });
  console.log(JSON.parse(rs));
});
